#!perl

use strict;
use warnings;

use Test::More;
use Wurm::mob;

my $meal = Wurm::mob->new({env => { }});
isa_ok($meal, 'Wurm::mob');
ok($meal->vent->{_mob}, 'the slice of life');

done_testing;
